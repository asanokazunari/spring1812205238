package com.sptpc.learnspring.model;

import lombok.*;
import org.hibernate.annotations.Type;
import org.joda.money.Money;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "t_drink")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Drink extends BaseEntity implements Serializable {

    @ManyToOne
    private Category category;
    private String name;

    @Type(type = "org.jadira.usertype.moneyandcurrency.joda.PersistentMoneyMinorAmount",
    parameters = {@org.hibernate.annotations.Parameter(name = "currencyCode",value = "CNY")})
    private Money price;

//    @ManyToMany
//    @JoinTable(name = "t_order_drink",joinColumns = {@JoinColumn(name = "items_id")})
//    private List<Order> drinkOrder;

    @Override
    public String toString() {
        return "Drink{" +
                "category=" + category +
                ", name='" + name + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }
}
