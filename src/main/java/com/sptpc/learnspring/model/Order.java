package com.sptpc.learnspring.model;


import com.sptpc.learnspring.common.OrderState;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "t_order")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order extends BaseEntity implements Serializable {
    @ManyToMany
    @JoinTable(name ="t_order_drink",joinColumns = {@JoinColumn(name = "drink_order_id")})
    @OrderBy("createTime")
    private List<Drink> items;
    private String customer;
    @Enumerated
    @Column(nullable = false)
    private OrderState state;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + super.getId() +
                ", customer='" + customer + '\'' +
                ", state=" + state +
                ",items=" + items +
                "} ";
    }
}
