package com.sptpc.learnspring.common;

public enum OrderState {
    INIT,PAID,BREWING,TAKEN,CANCELED
}
