package com.sptpc.learnspring.service;


import com.sptpc.learnspring.model.Category;
import com.sptpc.learnspring.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    public Category savaCategory(String name){
      return categoryRepository.save(Category.builder()
              .name(name).build());
    }
    public List<Category> finalAll(){
        return categoryRepository.findAll(Sort.by(Sort.Direction.DESC,"createTime"));
    }
//    public Category findOne(String name){
//        ExampleMatcher matcher =ExampleMatcher.matching()
//                .withIgnoreCase("name");
//        Optional<Category> category=categoryRepository
//                .findOne(Example.of(Category.builder().name(name).build(),matcher));
//        if (category.isPresent()){
//            return category.get();
//        }
//        else {
//            return null;
//        }
//    }
    public Category findOne(String name){
        Optional<Category> category= categoryRepository.findCategoryByNameLike(name);
        if (category.isPresent()){
            return category.get();
    }
        else {
        return null;
    }

}

    public Category upadate(long id, String newName) {
        Optional<Category> category=categoryRepository.findById(id);
    if (category.isPresent()){
        Category oldCatory =category.get();
        oldCatory.setName(newName);
        return categoryRepository.saveAndFlush(oldCatory);
    }
    else {
        return null;
    }
    }

    public boolean delete(long id) {
        try {
            categoryRepository.deleteById(id);
            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
