package com.sptpc.learnspring.service;

import com.sptpc.learnspring.model.Drink;
import com.sptpc.learnspring.repository.DrinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DrinkService {
    @Autowired
    private DrinkRepository drinkRepository;

    public Drink saveDrink(Drink drink){
        return drinkRepository.save(drink);
    }

    public Drink getDrink(String name){
        Optional<Drink> drink=drinkRepository.getDrinkByNameLike(name);
        if (drink.isPresent()){
            return drink.get();
        } else {
            return null;
        }
    }
    public List<Drink> getAllDrinks(long categoryId){ return drinkRepository.getDrinksByCategory_Id(categoryId); }

    public Drink updateDrink(Drink drink){
        return drinkRepository.saveAndFlush(drink);
    }

    public void delete(Drink drink){drinkRepository.delete(drink);}

}
