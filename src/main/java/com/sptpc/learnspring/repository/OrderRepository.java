package com.sptpc.learnspring.repository;


import com.sptpc.learnspring.model.Drink;
import com.sptpc.learnspring.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Long> {
    List<Order> findOrdersByCustomerLike(String customer);
    Long countOrdersByItemsName(String drinkName);
}
