package com.sptpc.learnspring.repository;

import com.sptpc.learnspring.model.Category;
import com.sptpc.learnspring.model.Drink;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DrinkRepository extends JpaRepository<Drink,Long> {
    List<Drink>getDrinksByCategory_Id(Long categoryId);
    Optional<Drink>getDrinkByNameLike(String name);
}
