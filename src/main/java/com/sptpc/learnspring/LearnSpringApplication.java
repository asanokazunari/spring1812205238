package com.sptpc.learnspring;

import com.sptpc.learnspring.common.OrderState;
import com.sptpc.learnspring.model.Category;
import com.sptpc.learnspring.model.Drink;
import com.sptpc.learnspring.model.Order;
import com.sptpc.learnspring.service.CategoryService;
import com.sptpc.learnspring.service.DrinkService;
import com.sptpc.learnspring.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@Slf4j
@SpringBootApplication
public class LearnSpringApplication implements ApplicationRunner {

    @Autowired
    private CategoryService categoryService;
    @Autowired
    private DrinkService drinkService;
    @Autowired
    private OrderService orderService;

    public static void main(String[] args) {
        SpringApplication.run(LearnSpringApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Category search = categoryService.findOne("咖啡");
        Drink latte =drinkService.saveDrink(
                Drink.builder().name("拿铁")
                        .price(Money.of(CurrencyUnit.of("CNY"),20.5))
                        .category(search).build()
        );
        log.info("创建一个新的饮品:{}",latte);
    }

    public void orderCRUD(){
        Drink drink1=drinkService.getDrink("西瓜汁");
        Drink drink2=drinkService.getDrink("柠檬汁");
        Order order=orderService.saveOrder("张三",drink1,drink2);
        log.info("订单创建成功:{}",order);

        List<Order>orders=orderService.getOrders("张三");
        orders.forEach(o -> log.info("订单信息：{}",o));

        Drink deleteDrink=drinkService.getDrink("西瓜汁");
        Order updateOrder=orderService.updateOrder(5L,deleteDrink);
        log.info("更新后的订单:{}",updateOrder);

        orderService.delete(5L);
    }

    private void drinkCRUD(){
        Category search = categoryService.findOne("果汁");
        Drink watermolen =drinkService.saveDrink(
                Drink.builder().name("西瓜汁")
                        .price(Money.of(CurrencyUnit.of("CNY"),8.0))
                        .category(search).build()
        );
        log.info("创建一个新的饮品:{}",watermolen);

        Category search2= categoryService.findOne("果汁");
        List<Drink> drinks=drinkService.getAllDrinks(search2.getId());
        drinks.forEach(drink -> log.info("包含饮品:{}",drink));

        Category newCategory=categoryService.findOne("咖啡");
        Drink oldDrink=drinkService.getDrink("西瓜汁");
        oldDrink.setCategory(newCategory);
        Drink updatedDrink=drinkService.updateDrink(oldDrink);
        log.info("更新了饮品:{}",updatedDrink);

        drinkService.delete(drinkService.getDrink("西瓜汁"));
    }

    public void categoryCRUD() {
        Category category = categoryService.savaCategory("果汁");
        log.info("创建一个新的分类:{}", category);

        List<Category> categoryList = categoryService.finalAll();
        categoryList.forEach(c -> {
            log.info("分类:{}", c);
        });

        Category search = categoryService.findOne("果汁");
        log.info("查找分类{}", search);

        Category update = categoryService.upadate(1L, "鲜榨果汁");
        log.info("更新分类{}", update);

        if (categoryService.delete(1L)) {
            log.info("删除成功");
        } else {
            log.error("删除失败");
        }
    }
}
